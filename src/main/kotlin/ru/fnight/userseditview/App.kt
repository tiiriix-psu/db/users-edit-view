package ru.fnight.userseditview

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.Alert
import javafx.stage.Stage
import ru.fnight.userseditview.controllers.Window

class App : Application(){
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(App::class.java)
        }

    }
    override fun start(stage: Stage) {
        val fxmlFile = "/fxml/window.fxml"
        val loader = FXMLLoader(javaClass.getResource(fxmlFile))
        val root = loader.load<Parent>()
        stage.title = "Редактирование пользователей"
        stage.scene = Scene(root)
        val controller = loader.getController<Window>()
        val connection = Utils.createConnection()
        if (connection == null) {
            val alert = Alert(Alert.AlertType.ERROR, "Не удалось установить подключение")
            alert.show() } else {
            controller.connection = connection
            controller.usersList.selectionModel.selectedItemProperty().addListener { observable, oldValue, newValue ->
                controller.onSelectUser(newValue)
            }
            controller.passwordText.focusedProperty().addListener { observable, oldValue, newValue ->
                controller.onFocusPassword(newValue)
            }
            controller.nameText.textProperty().addListener { observable, oldValue, newValue ->
                controller.onChangeLogin(newValue)
            }
            controller.registrationText.textProperty().addListener { observable, oldValue, newValue ->
                controller.onChangeTimestamp(newValue)
            }
            controller.passwordText.textProperty().addListener { observable, oldValue, newValue ->
                controller.onChangePassword(newValue)
            }
            controller.loadUsers()
            stage.show()
        }
    }
}