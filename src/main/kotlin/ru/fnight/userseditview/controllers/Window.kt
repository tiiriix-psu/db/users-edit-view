package ru.fnight.userseditview.controllers

import javafx.event.ActionEvent
import javafx.scene.control.Alert
import javafx.scene.control.Button
import javafx.scene.control.ListView
import javafx.scene.control.TextField
import org.mindrot.jbcrypt.BCrypt
import ru.fnight.userseditview.domains.User
import java.sql.Connection
import java.sql.Statement
import java.sql.Timestamp
import java.time.LocalDateTime

class Window {

    lateinit var nameText: TextField
    lateinit var registrationText: TextField
    lateinit var passwordText: TextField
    lateinit var usersList: ListView<User>
    lateinit var saveButton: Button
    lateinit var deleteButton: Button

    var users = ArrayList<User>()

    lateinit var connection: Connection

    var selectedUser: User? = null
    var passwordChange = false

    fun saveCurrent(action: ActionEvent) {
        if (selectedUser == null) {
            return
        }

        selectedUser!!.login = nameText.text
        try {
            selectedUser!!.registration = Timestamp.valueOf(registrationText.text)
        } catch (e: IllegalArgumentException) {
            Alert(Alert.AlertType.ERROR, "Неверный формат даты").show()
            return
        }

        if (selectedUser!!.isNew) {
            selectedUser!!.hash = BCrypt.hashpw(passwordText.text, BCrypt.gensalt())
            val sql = "insert into users (login, registration_date, passwd) values (?, ?, ?)"
            val statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
            statement.setString(1, selectedUser!!.login)
            statement.setTimestamp(2, selectedUser!!.registration)
            statement.setString(3, selectedUser!!.hash)
            statement.execute()
            val generatedKeys = statement.generatedKeys
            if (generatedKeys.next()) {
                selectedUser!!.id = generatedKeys.getLong(1)
            }
            selectedUser!!.isNew = false
        } else {
            if (passwordChange) {
                selectedUser!!.hash = BCrypt.hashpw(passwordText.text, BCrypt.gensalt())
                val sql = "update users set login = ?, passwd = ?, registration_date = ? where id = ?"
                val statement = connection.prepareStatement(sql)
                statement.setString(1, selectedUser!!.login)
                statement.setString(2, selectedUser!!.hash)
                statement.setTimestamp(3, selectedUser!!.registration)
                statement.setLong(4, selectedUser!!.id)
                statement.execute()
            } else {
                val sql = "update users set login = ?, registration_date = ? where id = ?"
                val statement = connection.prepareStatement(sql)
                statement.setString(1, selectedUser!!.login)
                statement.setTimestamp(2, selectedUser!!.registration)
                statement.setLong(3, selectedUser!!.id)
                statement.execute()
            }
        }
        usersList.refresh()
    }

    fun createNew(action: ActionEvent) {
        val newUser = User(-1, "", Timestamp.valueOf(LocalDateTime.now()), "")
        newUser.isNew = true
        users.add(newUser)
        usersList.items.add(newUser)
        selectedUser = newUser
        usersList.selectionModel.select(newUser)
        usersList.refresh()
        passwordText.text = ""
    }

    fun onFocusPassword(isFocus: Boolean) {
        if (isFocus && !passwordChange) {
            passwordText.text = ""
            passwordChange = true
        }
    }

    fun tryToUnlockButton() {
        val loginGood = nameText.text.isNotBlank()
        val passwordGood = passwordText.text.isNotBlank() ||
                (!passwordChange && if (selectedUser == null) true else !selectedUser!!.isNew)
        val timestampGood = registrationText.text.isNotBlank()
        saveButton.isDisable = !(loginGood && passwordGood && timestampGood)
    }

    fun onChangeLogin(newLogin: String) {
        tryToUnlockButton()
    }

    fun onChangeTimestamp(newStrTimestamp: String) {
        tryToUnlockButton()
    }

    fun onChangePassword(newPassword: String) {
        tryToUnlockButton()
    }

    fun onSelectUser(newSelectUser: User) {
        selectedUser = newSelectUser
        nameText.text = newSelectUser.login
        registrationText.text = newSelectUser.registration.toString()
        passwordText.text = "********"
        saveButton.isDisable = true
        deleteButton.isDisable = false
        passwordChange = false
    }

    fun loadUsers() {
        val sql = "select id, login, registration_date, passwd from users"
        val statement = connection.prepareStatement(sql)
        val resultSet = statement.executeQuery()

        users.clear()

        while (resultSet.next()) {
            val id = resultSet.getLong("id")
            val login = resultSet.getString("login")
            val registration = resultSet.getTimestamp("registration_date")
            val passwd = resultSet.getString("passwd")
            users.add(User(id, login, registration, passwd))
        }

        for (user in users) {
            usersList.items.add(user)
        }
    }

    fun delete(actionEvent: ActionEvent) {
        if (selectedUser == null) {
            return
        }
        val sql = "delete from users where id = ?"
        val statement = connection.prepareStatement(sql)
        statement.setLong(1, selectedUser!!.id)
        statement.execute()
        users.remove(selectedUser!!)
        saveButton.isDisable = true
        deleteButton.isDisable = true
        usersList.items.remove(selectedUser)
        usersList.refresh()
        passwordText.text = ""
        nameText.text = ""
        registrationText.text = ""
    }
}