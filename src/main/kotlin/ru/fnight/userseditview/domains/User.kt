package ru.fnight.userseditview.domains

import java.sql.Timestamp

class User(
        var id: Long,
        var login: String,
        var registration: Timestamp,
        var hash: String
) {

    var password: String? = null
    var isNew = false

    constructor(id: Long,
                login: String,
                registration: Timestamp,
                hash: String,
                password: String) : this(id, login, registration, hash) {
        this.password = password
    }

    override fun toString(): String {
        return login
    }
}